/**
 * Automatically generated file. DO NOT MODIFY
 */
package co.velloso.android.behaviors;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "co.velloso.android.behaviors";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
