package co.velli.pratik.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u001d\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0007\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\t\u001a\u00020\u00042\b\u0010\n\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u000b\u001a\u00020\u00042\b\u0010\f\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\r\u001a\u00020\u00042\b\u0010\u000e\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u000f\u001a\u00020\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0011\u001a\u00020\u00042\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0013\u001a\u00020\u00042\b\u0010\u0014\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0015\u001a\u00020\u00042\b\u0010\u0016\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0017\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0019\u001a\u00020\u00042\b\u0010\u001a\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u001b\u001a\u00020\u00042\b\u0010\u001c\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u001d\u001a\u00020\u00042\b\u0010\u001e\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u001f\u001a\u00020\u00042\b\u0010 \u001a\u0004\u0018\u00010\u0006J\u0010\u0010!\u001a\u00020\u00042\b\u0010\"\u001a\u0004\u0018\u00010\u0006\u00a8\u0006#"}, d2 = {"Lco/velli/pratik/extensions/ValidationUtil;", "", "()V", "isValidAddressComplement", "", "complement", "", "isValidAddressNumber", "number", "isValidCellPhone", "cellphone", "isValidCity", "city", "isValidCorporateName", "corporateName", "isValidCreditCardHolderName", "creditCardHolderName", "isValidCreditCardNumber", "creditCardNumer", "isValidCreditCardSecurityNumber", "creditCardSecurityNumber", "isValidMessage", "message", "isValidNeighborhood", "neighborhood", "isValidPhone", "phone", "isValidState", "state", "isValidStateRegistry", "stateRegistry", "isValidStreet", "address", "isValidZipCode", "zipCode", "toolbox_debug"})
public final class ValidationUtil {
    public static final co.velli.pratik.extensions.ValidationUtil INSTANCE = null;
    
    public final boolean isValidPhone(@org.jetbrains.annotations.Nullable()
    java.lang.String phone) {
        return false;
    }
    
    public final boolean isValidCellPhone(@org.jetbrains.annotations.Nullable()
    java.lang.String cellphone) {
        return false;
    }
    
    public final boolean isValidCorporateName(@org.jetbrains.annotations.Nullable()
    java.lang.String corporateName) {
        return false;
    }
    
    public final boolean isValidStateRegistry(@org.jetbrains.annotations.Nullable()
    java.lang.String stateRegistry) {
        return false;
    }
    
    public final boolean isValidZipCode(@org.jetbrains.annotations.Nullable()
    java.lang.String zipCode) {
        return false;
    }
    
    public final boolean isValidState(@org.jetbrains.annotations.Nullable()
    java.lang.String state) {
        return false;
    }
    
    public final boolean isValidCity(@org.jetbrains.annotations.Nullable()
    java.lang.String city) {
        return false;
    }
    
    public final boolean isValidNeighborhood(@org.jetbrains.annotations.Nullable()
    java.lang.String neighborhood) {
        return false;
    }
    
    public final boolean isValidStreet(@org.jetbrains.annotations.Nullable()
    java.lang.String address) {
        return false;
    }
    
    public final boolean isValidAddressNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String number) {
        return false;
    }
    
    public final boolean isValidAddressComplement(@org.jetbrains.annotations.Nullable()
    java.lang.String complement) {
        return false;
    }
    
    public final boolean isValidMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String message) {
        return false;
    }
    
    public final boolean isValidCreditCardHolderName(@org.jetbrains.annotations.Nullable()
    java.lang.String creditCardHolderName) {
        return false;
    }
    
    public final boolean isValidCreditCardNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String creditCardNumer) {
        return false;
    }
    
    public final boolean isValidCreditCardSecurityNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String creditCardSecurityNumber) {
        return false;
    }
    
    private ValidationUtil() {
        super();
    }
}