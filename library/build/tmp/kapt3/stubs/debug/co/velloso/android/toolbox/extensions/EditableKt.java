package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0006\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0002\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0004\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0005\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0006\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0007\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\b\u001a\u00020\t*\u00020\u0001\u00a8\u0006\n"}, d2 = {"formatCep", "Landroid/text/Editable;", "formatCnpj", "formatCpf", "formatCreditCard", "formatTaxPayerId", "formatValidThrough", "removeNonNumeric", "toDoubleLocale", "", "toolbox_debug"})
public final class EditableKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final android.text.Editable formatTaxPayerId(@org.jetbrains.annotations.NotNull()
    android.text.Editable $receiver) {
        return null;
    }
    
    public static final double toDoubleLocale(@org.jetbrains.annotations.NotNull()
    android.text.Editable $receiver) {
        return 0.0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.text.Editable removeNonNumeric(@org.jetbrains.annotations.NotNull()
    android.text.Editable $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.text.Editable formatCpf(@org.jetbrains.annotations.NotNull()
    android.text.Editable $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.text.Editable formatCnpj(@org.jetbrains.annotations.NotNull()
    android.text.Editable $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.text.Editable formatCep(@org.jetbrains.annotations.NotNull()
    android.text.Editable $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.text.Editable formatCreditCard(@org.jetbrains.annotations.NotNull()
    android.text.Editable $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.text.Editable formatValidThrough(@org.jetbrains.annotations.NotNull()
    android.text.Editable $receiver) {
        return null;
    }
}