package co.velloso.android.toolbox.common;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\b&\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u001d\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00028\u00002\u0006\u0010\u0007\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\bJ\u001d\u0010\t\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00028\u00002\u0006\u0010\u0007\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\bJ\u0015\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\r\u00a8\u0006\u000e"}, d2 = {"Lco/velloso/android/toolbox/common/SimpleDiffer;", "T", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "()V", "areContentsTheSame", "", "oldItem", "newItem", "(Ljava/lang/Object;Ljava/lang/Object;)Z", "areItemsTheSame", "getItemId", "", "item", "(Ljava/lang/Object;)J", "toolbox_debug"})
public abstract class SimpleDiffer<T extends java.lang.Object> extends androidx.recyclerview.widget.DiffUtil.ItemCallback<T> {
    
    public abstract long getItemId(T item);
    
    @java.lang.Override()
    public boolean areItemsTheSame(T oldItem, T newItem) {
        return false;
    }
    
    @java.lang.Override()
    public boolean areContentsTheSame(T oldItem, T newItem) {
        return false;
    }
    
    public SimpleDiffer() {
        super();
    }
}