package co.velloso.android.toolbox.volley;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0002\u00a8\u0006\u0005"}, d2 = {"imageLoader", "Lcom/android/volley/toolbox/ImageLoader;", "Landroid/content/Context;", "requestQueue", "Lcom/android/volley/RequestQueue;", "toolbox_debug"})
public final class ContextKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.android.volley.toolbox.ImageLoader imageLoader(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.android.volley.RequestQueue requestQueue(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver) {
        return null;
    }
}