package co.velloso.android.toolbox.widget.searchdebounce;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0016\u0018\u00002\u00020\u0001:\u0001/B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0006\u0010\u001d\u001a\u00020\rJ\b\u0010\u001e\u001a\u00020\u001fH\u0014J\b\u0010 \u001a\u00020\u001fH\u0014JU\u0010!\u001a\u00020\u001f2M\u0010\u0010\u001aI\u0012\u0013\u0012\u00110#\u00a2\u0006\f\b$\u0012\b\b%\u0012\u0004\b\b(&\u0012\u0013\u0012\u00110\'\u00a2\u0006\f\b$\u0012\b\b%\u0012\u0004\b\b((\u0012\u0013\u0012\u00110\r\u00a2\u0006\f\b$\u0012\b\b%\u0012\u0004\b\b()\u0012\u0004\u0012\u00020\u001f\u0018\u00010\"J\u0012\u0010*\u001a\u00020\u001f2\b\u0010\u0010\u001a\u0004\u0018\u00010+H\u0016J\u001a\u0010,\u001a\u00020\u001f2\b\u0010-\u001a\u0004\u0018\u00010\'2\u0006\u0010.\u001a\u00020\rH\u0016R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\r@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001c\u00a8\u00060"}, d2 = {"Lco/velloso/android/toolbox/widget/searchdebounce/DebounceSearchView;", "Landroidx/appcompat/widget/SearchView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "debounceCounter", "disposable", "Lio/reactivex/disposables/Disposable;", "<set-?>", "", "isQueryFocused", "()Z", "listener", "Lco/velloso/android/toolbox/widget/searchdebounce/DebounceSearchView$SearchDebounceViewListener;", "maxDebounce", "getMaxDebounce", "()I", "setMaxDebounce", "(I)V", "timeDebounce", "", "getTimeDebounce", "()J", "setTimeDebounce", "(J)V", "hasQueryFocus", "onAttachedToWindow", "", "onDetachedFromWindow", "setOnDebouncedQueryListener", "Lkotlin/Function3;", "Landroid/view/View;", "Lkotlin/ParameterName;", "name", "view", "", "queryText", "isSubmitted", "setOnQueryTextFocusChangeListener", "Landroid/view/View$OnFocusChangeListener;", "setQuery", "query", "submit", "SearchDebounceViewListener", "toolbox_debug"})
public class DebounceSearchView extends androidx.appcompat.widget.SearchView {
    private int maxDebounce;
    private long timeDebounce;
    private io.reactivex.disposables.Disposable disposable;
    private int debounceCounter;
    private co.velloso.android.toolbox.widget.searchdebounce.DebounceSearchView.SearchDebounceViewListener listener;
    private boolean isQueryFocused;
    private java.util.HashMap _$_findViewCache;
    
    public final int getMaxDebounce() {
        return 0;
    }
    
    public final void setMaxDebounce(int p0) {
    }
    
    public final long getTimeDebounce() {
        return 0L;
    }
    
    public final void setTimeDebounce(long p0) {
    }
    
    public final boolean isQueryFocused() {
        return false;
    }
    
    public final boolean hasQueryFocus() {
        return false;
    }
    
    @java.lang.Override()
    public void setOnQueryTextFocusChangeListener(@org.jetbrains.annotations.Nullable()
    android.view.View.OnFocusChangeListener listener) {
    }
    
    @java.lang.Override()
    protected void onAttachedToWindow() {
    }
    
    @java.lang.Override()
    protected void onDetachedFromWindow() {
    }
    
    /**
     * * Sets a query string in the text field and optionally submits the query as well.
     *     *
     *     * @param submit if true, immediately call onDebouncedQuery listener
     */
    @java.lang.Override()
    public void setQuery(@org.jetbrains.annotations.Nullable()
    java.lang.CharSequence query, boolean submit) {
    }
    
    public final void setOnDebouncedQueryListener(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function3<? super android.view.View, ? super java.lang.CharSequence, ? super java.lang.Boolean, kotlin.Unit> listener) {
    }
    
    public DebounceSearchView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs, int defStyleAttr) {
        super(null);
    }
    
    public DebounceSearchView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs) {
        super(null);
    }
    
    public DebounceSearchView(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH&\u00a8\u0006\n"}, d2 = {"Lco/velloso/android/toolbox/widget/searchdebounce/DebounceSearchView$SearchDebounceViewListener;", "", "onDebouncedQuery", "", "view", "Landroid/view/View;", "queryText", "", "isSubmitted", "", "toolbox_debug"})
    public static abstract interface SearchDebounceViewListener {
        
        public abstract void onDebouncedQuery(@org.jetbrains.annotations.NotNull()
        android.view.View view, @org.jetbrains.annotations.NotNull()
        java.lang.CharSequence queryText, boolean isSubmitted);
    }
}