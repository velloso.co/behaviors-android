package co.velloso.android.toolbox.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u001f\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\n\b\u0000\u0010\u0002\u0018\u0001*\u00020\u0003*\u00020\u0004H\u0086\b\u001a1\u0010\u0005\u001a\u0002H\u0002\"\b\b\u0000\u0010\u0002*\u00020\u0003*\u00020\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\t\u001a1\u0010\u0005\u001a\u0002H\u0002\"\b\b\u0000\u0010\u0002*\u00020\u0003*\u00020\n2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000b\u001a\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\n\b\u0000\u0010\u0002\u0018\u0001*\u00020\u0003*\u00020\u0004H\u0086\b\u001a\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\n\b\u0000\u0010\u0002\u0018\u0001*\u00020\u0003*\u00020\nH\u0086\b\u00a8\u0006\r"}, d2 = {"activityViewModel", "Lkotlin/Lazy;", "T", "Landroidx/lifecycle/ViewModel;", "Landroidx/fragment/app/Fragment;", "getViewModel", "modelClass", "Lkotlin/reflect/KClass;", "owner", "(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Landroidx/fragment/app/Fragment;)Landroidx/lifecycle/ViewModel;", "Landroidx/fragment/app/FragmentActivity;", "(Landroidx/fragment/app/FragmentActivity;Lkotlin/reflect/KClass;Landroidx/fragment/app/FragmentActivity;)Landroidx/lifecycle/ViewModel;", "viewModel", "toolbox_debug"})
public final class LifecycleOwnerKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends androidx.lifecycle.ViewModel>T getViewModel(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentActivity $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> modelClass, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentActivity owner) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends androidx.lifecycle.ViewModel>T getViewModel(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<T> modelClass, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment owner) {
        return null;
    }
    
    private static final <T extends androidx.lifecycle.ViewModel>kotlin.Lazy<T> viewModel(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentActivity $receiver) {
        return null;
    }
    
    private static final <T extends androidx.lifecycle.ViewModel>kotlin.Lazy<T> viewModel(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $receiver) {
        return null;
    }
    
    private static final <T extends androidx.lifecycle.ViewModel>kotlin.Lazy<T> activityViewModel(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $receiver) {
        return null;
    }
}