package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010&\n\u0002\b\u0003\u001a4\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00060\u0005H\u0086\b\u00a2\u0006\u0002\u0010\u0007\u001a7\u0010\b\u001a\u0004\u0018\u0001H\t\"\u0004\b\u0000\u0010\t\"\u0004\b\u0001\u0010\n*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\t\u0012\u0004\u0012\u0002H\n0\u000b0\u00032\u0006\u0010\f\u001a\u0002H\n\u00a2\u0006\u0002\u0010\r\u00a8\u0006\u000e"}, d2 = {"findIndex", "", "T", "", "predicate", "Lkotlin/Function1;", "", "(Ljava/lang/Iterable;Lkotlin/jvm/functions/Function1;)Ljava/lang/Integer;", "findKey", "K", "V", "", "value", "(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;", "toolbox_debug"})
public final class FunctionalKt {
    
    /**
     * * Functional extensions
     */
    @org.jetbrains.annotations.Nullable()
    public static final <T extends java.lang.Object>java.lang.Integer findIndex(@org.jetbrains.annotations.NotNull()
    java.lang.Iterable<? extends T> $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super T, java.lang.Boolean> predicate) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public static final <K extends java.lang.Object, V extends java.lang.Object>K findKey(@org.jetbrains.annotations.NotNull()
    java.lang.Iterable<? extends java.util.Map.Entry<? extends K, ? extends V>> $receiver, V value) {
        return null;
    }
}