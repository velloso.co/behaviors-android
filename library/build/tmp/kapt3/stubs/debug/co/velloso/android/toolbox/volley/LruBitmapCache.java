package co.velloso.android.toolbox.volley;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u0000 \u00132\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004:\u0001\u0013B\u000f\b\u0016\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\r\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u00032\u0006\u0010\f\u001a\u00020\u0002H\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\f\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u0003H\u0016J\u0018\u0010\u0010\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0003H\u0014\u00a8\u0006\u0014"}, d2 = {"Lco/velloso/android/toolbox/volley/LruBitmapCache;", "Landroid/util/LruCache;", "", "Landroid/graphics/Bitmap;", "Lcom/android/volley/toolbox/ImageLoader$ImageCache;", "ctx", "Landroid/content/Context;", "(Landroid/content/Context;)V", "maxSize", "", "(I)V", "getBitmap", "url", "putBitmap", "", "bitmap", "sizeOf", "key", "value", "Companion", "toolbox_debug"})
public final class LruBitmapCache extends android.util.LruCache<java.lang.String, android.graphics.Bitmap> implements com.android.volley.toolbox.ImageLoader.ImageCache {
    public static final co.velloso.android.toolbox.volley.LruBitmapCache.Companion Companion = null;
    
    @java.lang.Override()
    protected int sizeOf(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap value) {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.graphics.Bitmap getBitmap(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
        return null;
    }
    
    @java.lang.Override()
    public void putBitmap(@org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap bitmap) {
    }
    
    public LruBitmapCache(int maxSize) {
        super(0);
    }
    
    public LruBitmapCache(@org.jetbrains.annotations.NotNull()
    android.content.Context ctx) {
        super(0);
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lco/velloso/android/toolbox/volley/LruBitmapCache$Companion;", "", "()V", "getCacheSize", "", "ctx", "Landroid/content/Context;", "toolbox_debug"})
    public static final class Companion {
        
        public final int getCacheSize(@org.jetbrains.annotations.NotNull()
        android.content.Context ctx) {
            return 0;
        }
        
        private Companion() {
            super();
        }
    }
}