package co.velloso.android.toolbox.widget.statefulrecycler;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0016J\u0016\u0010\n\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0016\u00a8\u0006\u000b"}, d2 = {"Lco/velloso/android/toolbox/widget/statefulrecycler/StatefulViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "onRestoreItemViewHolderState", "", "state", "Landroid/util/SparseArray;", "Landroid/os/Parcelable;", "onSaveItemViewHolderState", "toolbox_debug"})
public class StatefulViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
    
    /**
     * *
     *     * This is called after super RecyclerView.Adapter#onViewDetachedFromWindow().
     *     *
     *     * Override this if it's not necessary to save all the view state, for example, if only scroll
     *     * views need their state saved, override this with:
     *     * ```
     *     *     itemView.findViewById(R.id.myScrollView).saveHierarchyState(state)
     *     * ```
     */
    public void onSaveItemViewHolderState(@org.jetbrains.annotations.NotNull()
    android.util.SparseArray<android.os.Parcelable> state) {
    }
    
    /**
     * *
     *     * This is called after the RecyclerView.Adapter#onBindViewHolder() so if the adapter
     *     * data changes while the holder is not visible, the itemView state saved TODO has to be
     *     * invalidated.
     *     *
     *     * Override this if it's not necessary to restore all the view state, for example, if only a
     *     * scroll view need its state saved, override this with:
     *     * ```
     *     *     itemView.findViewById(R.id.myScrollView).restoreHierarchyState(state)
     *     * ```
     */
    public void onRestoreItemViewHolderState(@org.jetbrains.annotations.NotNull()
    android.util.SparseArray<android.os.Parcelable> state) {
    }
    
    public StatefulViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.View itemView) {
        super(null);
    }
}