package co.velloso.android.toolbox.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a8\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0003\"\u0004\b\u0001\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00030\u00012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00020\u0005H\u0007\u001a>\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0003\"\u0004\b\u0001\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00030\u00012\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00010\u0005H\u0007\u00a8\u0006\b"}, d2 = {"map", "Landroidx/lifecycle/LiveData;", "Y", "X", "mapFunction", "Lkotlin/Function1;", "switchMap", "switchMapFunction", "toolbox_debug"})
public final class LiveDataKt {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.annotation.MainThread()
    public static final <X extends java.lang.Object, Y extends java.lang.Object>androidx.lifecycle.LiveData<Y> map(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<X> $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super X, ? extends Y> mapFunction) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.annotation.MainThread()
    public static final <X extends java.lang.Object, Y extends java.lang.Object>androidx.lifecycle.LiveData<Y> switchMap(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<X> $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super X, ? extends androidx.lifecycle.LiveData<Y>> switchMapFunction) {
        return null;
    }
}