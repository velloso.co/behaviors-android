package co.velloso.android.toolbox.volley;

import java.lang.System;

/**
 * * A custom implementation of Volley Cache ImageLoader to force cache on images without HTTP header
 * * cache attribute.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0012B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J<\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u000bH\u0014\u00a8\u0006\u0013"}, d2 = {"Lco/velloso/android/toolbox/volley/CachedImageLoader;", "Lcom/android/volley/toolbox/ImageLoader;", "queue", "Lcom/android/volley/RequestQueue;", "imageCache", "Lcom/android/volley/toolbox/ImageLoader$ImageCache;", "(Lcom/android/volley/RequestQueue;Lcom/android/volley/toolbox/ImageLoader$ImageCache;)V", "makeImageRequest", "Lcom/android/volley/Request;", "Landroid/graphics/Bitmap;", "requestUrl", "", "maxWidth", "", "maxHeight", "scaleType", "Landroid/widget/ImageView$ScaleType;", "cacheKey", "ImageRequest", "toolbox_debug"})
public final class CachedImageLoader extends com.android.volley.toolbox.ImageLoader {
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected com.android.volley.Request<android.graphics.Bitmap> makeImageRequest(@org.jetbrains.annotations.Nullable()
    java.lang.String requestUrl, int maxWidth, int maxHeight, @org.jetbrains.annotations.Nullable()
    android.widget.ImageView.ScaleType scaleType, @org.jetbrains.annotations.Nullable()
    java.lang.String cacheKey) {
        return null;
    }
    
    public CachedImageLoader(@org.jetbrains.annotations.NotNull()
    com.android.volley.RequestQueue queue, @org.jetbrains.annotations.NotNull()
    com.android.volley.toolbox.ImageLoader.ImageCache imageCache) {
        super(null, null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B;\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\fJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014\u00a8\u0006\u0012"}, d2 = {"Lco/velloso/android/toolbox/volley/CachedImageLoader$ImageRequest;", "Lcom/android/volley/toolbox/ImageRequest;", "requestUrl", "", "maxWidth", "", "maxHeight", "scaleType", "Landroid/widget/ImageView$ScaleType;", "decodeConfig", "Landroid/graphics/Bitmap$Config;", "cacheKey", "(Lco/velloso/android/toolbox/volley/CachedImageLoader;Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;Landroid/graphics/Bitmap$Config;Ljava/lang/String;)V", "parseNetworkResponse", "Lcom/android/volley/Response;", "Landroid/graphics/Bitmap;", "response", "Lcom/android/volley/NetworkResponse;", "toolbox_debug"})
    public final class ImageRequest extends com.android.volley.toolbox.ImageRequest {
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        protected com.android.volley.Response<android.graphics.Bitmap> parseNetworkResponse(@org.jetbrains.annotations.NotNull()
        com.android.volley.NetworkResponse response) {
            return null;
        }
        
        public ImageRequest(@org.jetbrains.annotations.Nullable()
        java.lang.String requestUrl, int maxWidth, int maxHeight, @org.jetbrains.annotations.Nullable()
        android.widget.ImageView.ScaleType scaleType, @org.jetbrains.annotations.NotNull()
        android.graphics.Bitmap.Config decodeConfig, @org.jetbrains.annotations.Nullable()
        java.lang.String cacheKey) {
            super(null, null, 0, 0, null, null, null);
        }
    }
}