package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000$\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a$\u0010\u0003\u001a\u00020\u0004*\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0007\u001a\"\u0010\b\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\f\u001a,\u0010\r\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\fH\u0007\u001a2\u0010\r\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\f\u001a\u0012\u0010\u0011\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\u000e\u001a\u00020\f\u00a8\u0006\u0012"}, d2 = {"daysFromToday", "", "Ljava/util/Date;", "toDateString", "", "pattern", "timeZone", "Ljava/util/TimeZone;", "toSimpleDateString", "todayString", "yesterdayString", "standardFormat", "Ljava/text/DateFormat;", "toSimpleDateTimeString", "timeFormat", "template", "dateFormat", "toSimpleTimeString", "toolbox_debug"})
public final class DateKt {
    
    /**
     * * Date extensions
     */
    public static final int daysFromToday(@org.jetbrains.annotations.NotNull()
    java.util.Date $receiver) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toSimpleDateString(@org.jetbrains.annotations.NotNull()
    java.util.Date $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String todayString, @org.jetbrains.annotations.NotNull()
    java.lang.String yesterdayString, @org.jetbrains.annotations.NotNull()
    java.text.DateFormat standardFormat) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SimpleDateFormat"})
    public static final java.lang.String toSimpleDateTimeString(@org.jetbrains.annotations.NotNull()
    java.util.Date $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String todayString, @org.jetbrains.annotations.NotNull()
    java.lang.String yesterdayString, @org.jetbrains.annotations.NotNull()
    java.text.DateFormat standardFormat, @org.jetbrains.annotations.NotNull()
    java.text.DateFormat timeFormat) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toSimpleTimeString(@org.jetbrains.annotations.NotNull()
    java.util.Date $receiver, @org.jetbrains.annotations.NotNull()
    java.text.DateFormat timeFormat) {
        return null;
    }
    
    /**
     * * @param template the template that concatenate date and time such as {@code $1%s at $2%s}
     * * @param todayString the word that represent today in the application language
     * * @param yesterdayString the word that represent yesterday in the application language
     * * @param dateFormat the DateFormat that should be used for the case it's not today or yesterday
     * * @param timeFormat the DateFormat that should be used for time part of string
     * *
     * * @return a simple datetime String such as {@code Yesterday at 13:00} or
     * * {@code Jan 1, 2017 at 14:34}.
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toSimpleDateTimeString(@org.jetbrains.annotations.NotNull()
    java.util.Date $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String template, @org.jetbrains.annotations.NotNull()
    java.lang.String todayString, @org.jetbrains.annotations.NotNull()
    java.lang.String yesterdayString, @org.jetbrains.annotations.NotNull()
    java.text.DateFormat dateFormat, @org.jetbrains.annotations.NotNull()
    java.text.DateFormat timeFormat) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SimpleDateFormat"})
    public static final java.lang.String toDateString(@org.jetbrains.annotations.NotNull()
    java.util.Date $receiver, @org.jetbrains.annotations.Nullable()
    java.lang.String pattern, @org.jetbrains.annotations.Nullable()
    java.util.TimeZone timeZone) {
        return null;
    }
}