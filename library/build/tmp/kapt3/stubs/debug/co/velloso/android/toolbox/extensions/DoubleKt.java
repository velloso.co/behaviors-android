package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0010\u000e\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\u001a\u001a\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0001\u001a\n\u0010\b\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\t\u001a\u00020\u0001*\u00020\u0002\u001a(\u0010\t\u001a\u00020\u0001*\u00020\u00022\b\u0010\n\u001a\u0004\u0018\u00010\u00012\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\f\u001a\n\u0010\u000e\u001a\u00020\u0001*\u00020\u0002\u001a\u0012\u0010\u000f\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0006\u00a8\u0006\u0011"}, d2 = {"toString", "", "", "resources", "Landroid/content/res/Resources;", "resourceId", "", "template", "toStringMinimal", "toStringPrice", "currencySymbol", "showDecimal", "", "prepend", "toStringPriceDecimal", "toStringQuantity", "digits", "toolbox_debug"})
public final class DoubleKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toStringPriceDecimal(double $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toStringPrice(double $receiver, @org.jetbrains.annotations.Nullable()
    java.lang.String currencySymbol, boolean showDecimal, boolean prepend) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toStringPrice(double $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toString(double $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String template) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toString(double $receiver, @org.jetbrains.annotations.NotNull()
    android.content.res.Resources resources, int resourceId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toStringQuantity(double $receiver, int digits) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toStringMinimal(double $receiver) {
        return null;
    }
}