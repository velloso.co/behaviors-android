package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a)\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u0002H\u0002\u00a2\u0006\u0002\u0010\u0004\u001a)\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u0002H\u0002\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0006"}, d2 = {"append", "Ljava/util/ArrayList;", "T", "item", "(Ljava/util/ArrayList;Ljava/lang/Object;)Ljava/util/ArrayList;", "prepend", "toolbox_debug"})
public final class ArrayListKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>java.util.ArrayList<T> prepend(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<T> $receiver, T item) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>java.util.ArrayList<T> append(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<T> $receiver, T item) {
        return null;
    }
}