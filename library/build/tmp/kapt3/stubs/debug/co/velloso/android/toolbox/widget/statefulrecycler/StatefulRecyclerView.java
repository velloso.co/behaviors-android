package co.velloso.android.toolbox.widget.statefulrecycler;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u000eB#\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0014J\n\u0010\r\u001a\u0004\u0018\u00010\fH\u0014\u00a8\u0006\u000f"}, d2 = {"Lco/velloso/android/toolbox/widget/statefulrecycler/StatefulRecyclerView;", "Landroidx/recyclerview/widget/RecyclerView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "onRestoreInstanceState", "", "state", "Landroid/os/Parcelable;", "onSaveInstanceState", "SavedState", "toolbox_debug"})
public final class StatefulRecyclerView extends androidx.recyclerview.widget.RecyclerView {
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected android.os.Parcelable onSaveInstanceState() {
        return null;
    }
    
    @java.lang.Override()
    protected void onRestoreInstanceState(@org.jetbrains.annotations.Nullable()
    android.os.Parcelable state) {
    }
    
    public StatefulRecyclerView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs, int defStyle) {
        super(null);
    }
    
    public StatefulRecyclerView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0019\b\u0010\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bB\u000f\b\u0010\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0015\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0000H\u0000\u00a2\u0006\u0002\b\u0013J\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u0018\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0015H\u0016R\u001c\u0010\f\u001a\u0004\u0018\u00010\nX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u000b\u00a8\u0006\u001a"}, d2 = {"Lco/velloso/android/toolbox/widget/statefulrecycler/StatefulRecyclerView$SavedState;", "Landroidx/customview/view/AbsSavedState;", "parcel", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "input", "loader", "Ljava/lang/ClassLoader;", "(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V", "superState", "Landroid/os/Parcelable;", "(Landroid/os/Parcelable;)V", "adapterState", "getAdapterState$toolbox_debug", "()Landroid/os/Parcelable;", "setAdapterState$toolbox_debug", "copyFrom", "", "other", "copyFrom$toolbox_debug", "describeContents", "", "writeToParcel", "dest", "flags", "Companion", "toolbox_debug"})
    public static final class SavedState extends androidx.customview.view.AbsSavedState {
        @org.jetbrains.annotations.Nullable()
        private android.os.Parcelable adapterState;
        @org.jetbrains.annotations.NotNull()
        public static final android.os.Parcelable.Creator<co.velloso.android.toolbox.widget.statefulrecycler.StatefulRecyclerView.SavedState> CREATOR = null;
        public static final co.velloso.android.toolbox.widget.statefulrecycler.StatefulRecyclerView.SavedState.Companion Companion = null;
        
        @org.jetbrains.annotations.Nullable()
        public final android.os.Parcelable getAdapterState$toolbox_debug() {
            return null;
        }
        
        public final void setAdapterState$toolbox_debug(@org.jetbrains.annotations.Nullable()
        android.os.Parcelable p0) {
        }
        
        @java.lang.Override()
        public void writeToParcel(@org.jetbrains.annotations.NotNull()
        android.os.Parcel dest, int flags) {
        }
        
        public final void copyFrom$toolbox_debug(@org.jetbrains.annotations.NotNull()
        co.velloso.android.toolbox.widget.statefulrecycler.StatefulRecyclerView.SavedState other) {
        }
        
        @java.lang.Override()
        public int describeContents() {
            return 0;
        }
        
        public SavedState(@org.jetbrains.annotations.NotNull()
        android.os.Parcel parcel) {
            super(null);
        }
        
        public SavedState(@org.jetbrains.annotations.NotNull()
        android.os.Parcel input, @org.jetbrains.annotations.Nullable()
        java.lang.ClassLoader loader) {
            super(null);
        }
        
        public SavedState(@org.jetbrains.annotations.NotNull()
        android.os.Parcelable superState) {
            super(null);
        }
        
        @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lco/velloso/android/toolbox/widget/statefulrecycler/StatefulRecyclerView$SavedState$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/velloso/android/toolbox/widget/statefulrecycler/StatefulRecyclerView$SavedState;", "toolbox_debug"})
        public static final class Companion {
            
            private Companion() {
                super();
            }
        }
    }
}