package co.velloso.android.toolbox.volley;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0010\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u00122\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u0002H\u00120\u0014R\u001b\u0010\u0005\u001a\u00020\u00068FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\f8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0016"}, d2 = {"Lco/velloso/android/toolbox/volley/VolleySingleton;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "imageLoader", "Lcom/android/volley/toolbox/ImageLoader;", "getImageLoader", "()Lcom/android/volley/toolbox/ImageLoader;", "imageLoader$delegate", "Lkotlin/Lazy;", "requestQueue", "Lcom/android/volley/RequestQueue;", "getRequestQueue", "()Lcom/android/volley/RequestQueue;", "requestQueue$delegate", "addToRequestQueue", "", "T", "request", "Lcom/android/volley/Request;", "Companion", "toolbox_debug"})
public final class VolleySingleton {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy imageLoader$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy requestQueue$delegate = null;
    private static volatile co.velloso.android.toolbox.volley.VolleySingleton INSTANCE;
    public static final int MAX_SIZE_CACHE = 20000000;
    public static final co.velloso.android.toolbox.volley.VolleySingleton.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.android.volley.toolbox.ImageLoader getImageLoader() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.android.volley.RequestQueue getRequestQueue() {
        return null;
    }
    
    public final <T extends java.lang.Object>void addToRequestQueue(@org.jetbrains.annotations.NotNull()
    com.android.volley.Request<T> request) {
    }
    
    public VolleySingleton(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lco/velloso/android/toolbox/volley/VolleySingleton$Companion;", "", "()V", "INSTANCE", "Lco/velloso/android/toolbox/volley/VolleySingleton;", "MAX_SIZE_CACHE", "", "getInstance", "context", "Landroid/content/Context;", "toolbox_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final co.velloso.android.toolbox.volley.VolleySingleton getInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}