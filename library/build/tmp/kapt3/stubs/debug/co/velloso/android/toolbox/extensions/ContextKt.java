package co.velloso.android.toolbox.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000H\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0012\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001*\u00020\u0003\u001a\n\u0010\u0004\u001a\u00020\u0005*\u00020\u0003\u001a\u0019\u0010\u0006\u001a\u0004\u0018\u00010\u0007*\u00020\u00032\u0006\u0010\b\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\t\u001a\u0014\u0010\n\u001a\u00020\u0007*\u00020\u00032\b\b\u0001\u0010\u000b\u001a\u00020\u0007\u001a\u0012\u0010\f\u001a\u00020\u0007*\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e\u001a\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u0010*\u00020\u00032\b\b\u0001\u0010\u000b\u001a\u00020\u0007\u001a\n\u0010\u0011\u001a\u00020\u0012*\u00020\u0003\u001a>\u0010\u0013\u001a\u0002H\u0014\"\u0004\b\u0000\u0010\u0014*\u00020\u00032\b\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u0002H\u00140\u001aH\u0086\b\u00a2\u0006\u0002\u0010\u001c\u00a8\u0006\u001d"}, d2 = {"getAccountManager", "Landroid/accounts/AccountManager;", "kotlin.jvm.PlatformType", "Landroid/content/Context;", "getAlarmManager", "Landroid/app/AlarmManager;", "getAttributeDimen", "", "attr", "(Landroid/content/Context;I)Ljava/lang/Integer;", "getColorCompat", "id", "getDpDimen", "dp", "", "getDrawableCompat", "Landroid/graphics/drawable/Drawable;", "getNotificationManager", "Landroidx/core/app/NotificationManagerCompat;", "useAttributes", "R", "attrs", "Landroid/util/AttributeSet;", "typedArrayIds", "", "block", "Lkotlin/Function1;", "Landroid/content/res/TypedArray;", "(Landroid/content/Context;Landroid/util/AttributeSet;[ILkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "toolbox_debug"})
public final class ContextKt {
    
    /**
     * * Android resources extensions
     */
    @org.jetbrains.annotations.Nullable()
    public static final java.lang.Integer getAttributeDimen(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver, int attr) {
        return null;
    }
    
    public static final int getDpDimen(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver, float dp) {
        return 0;
    }
    
    public static final <R extends java.lang.Object>R useAttributes(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs, @org.jetbrains.annotations.NotNull()
    int[] typedArrayIds, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super android.content.res.TypedArray, ? extends R> block) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public static final android.graphics.drawable.Drawable getDrawableCompat(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver, @androidx.annotation.DrawableRes()
    int id) {
        return null;
    }
    
    public static final int getColorCompat(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver, @androidx.annotation.ColorRes()
    int id) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.app.AlarmManager getAlarmManager(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final androidx.core.app.NotificationManagerCompat getNotificationManager(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver) {
        return null;
    }
    
    public static final android.accounts.AccountManager getAccountManager(@org.jetbrains.annotations.NotNull()
    android.content.Context $receiver) {
        return null;
    }
}