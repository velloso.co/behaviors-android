package co.velloso.android.toolbox.volley;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u001a.\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\b\u001a&\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\b\u00a8\u0006\n"}, d2 = {"setImageUrl", "", "Landroid/widget/ImageView;", "imageLoader", "Lcom/android/volley/toolbox/ImageLoader;", "imageUrl", "", "defaultDrawableId", "", "errorDrawableId", "toolbox_debug"})
public final class ImageViewKt {
    
    public static final void setImageUrl(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $receiver, @org.jetbrains.annotations.NotNull()
    com.android.volley.toolbox.ImageLoader imageLoader, @org.jetbrains.annotations.NotNull()
    java.lang.String imageUrl, int defaultDrawableId, int errorDrawableId) {
    }
    
    public static final void setImageUrl(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String imageUrl, int defaultDrawableId, int errorDrawableId) {
    }
}