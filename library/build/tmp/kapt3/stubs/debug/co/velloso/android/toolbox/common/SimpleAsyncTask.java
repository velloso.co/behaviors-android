package co.velloso.android.toolbox.common;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0011\n\u0002\b\u0003\u0018\u0000*\u0004\b\u0000\u0010\u00012\u0016\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u0001H\u00010\u0002B:\u0012\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0005\u0012#\u0010\u0006\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00018\u0000\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\u000b0\u0007\u00a2\u0006\u0002\u0010\fJ\'\u0010\u0014\u001a\u0004\u0018\u00018\u00002\u0016\u0010\u0015\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00030\u0016\"\u0004\u0018\u00010\u0003H\u0014\u00a2\u0006\u0002\u0010\u0017J\u0017\u0010\u0006\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00018\u0000H\u0014\u00a2\u0006\u0002\u0010\u0018R\"\u0010\r\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R+\u0010\u0006\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00018\u0000\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\u000b0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"}, d2 = {"Lco/velloso/android/toolbox/common/SimpleAsyncTask;", "R", "Landroid/os/AsyncTask;", "Ljava/lang/Void;", "onBackground", "Lkotlin/Function0;", "onPostExecute", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "result", "", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "getE", "()Ljava/lang/Exception;", "setE", "(Ljava/lang/Exception;)V", "doInBackground", "params", "", "([Ljava/lang/Void;)Ljava/lang/Object;", "(Ljava/lang/Object;)V", "toolbox_debug"})
public final class SimpleAsyncTask<R extends java.lang.Object> extends android.os.AsyncTask<java.lang.Void, java.lang.Void, R> {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Exception e;
    private final kotlin.jvm.functions.Function0<R> onBackground = null;
    private final kotlin.jvm.functions.Function1<R, kotlin.Unit> onPostExecute = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Exception getE() {
        return null;
    }
    
    public final void setE(@org.jetbrains.annotations.Nullable()
    java.lang.Exception p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected R doInBackground(@org.jetbrains.annotations.NotNull()
    java.lang.Void... params) {
        return null;
    }
    
    @java.lang.Override()
    protected void onPostExecute(@org.jetbrains.annotations.Nullable()
    R result) {
    }
    
    public SimpleAsyncTask(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends R> onBackground, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super R, kotlin.Unit> onPostExecute) {
        super();
    }
}