package co.velloso.android.behaviors

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlin.math.max


class HideBottomViewOnDragBehavior @JvmOverloads constructor(
        context: Context? = null,
        attrs: AttributeSet? = null
) : CoordinatorLayout.Behavior<BottomNavigationView>(context, attrs) {

    private var peekHeight = 0

    var maxOffset = 0

    override fun layoutDependsOn(parent: CoordinatorLayout, child: BottomNavigationView, dependency: View): Boolean {
        if (isBottomSheet(dependency)) {
            peekHeight = BottomSheetBehavior.from(dependency).peekHeight
            return true
        }
        return false
    }

    private fun isBottomSheet(dependency: View): Boolean {
        val params = dependency.layoutParams as? CoordinatorLayout.LayoutParams ?: return false
        return params.behavior is BottomSheetBehavior<*>
    }

    override fun onLayoutChild(parent: CoordinatorLayout, child: BottomNavigationView, layoutDirection: Int): Boolean {
        maxOffset = parent.height - peekHeight
        return super.onLayoutChild(parent, child, layoutDirection)
    }

    override fun onDependentViewChanged(
            parent: CoordinatorLayout,
            child: BottomNavigationView,
            dependency: View
    ): Boolean {
        if (isBottomSheet(dependency)) {
            val offset = maxOffset - dependency.top
            val progress = offset.toFloat() / maxOffset
            dispatchSlide(child, progress)
        }
        return super.onDependentViewChanged(parent, child, dependency)
    }

    private fun dispatchSlide(child: View, progress: Float) {
        child.translationY = max(child.height.toFloat().times(progress), 0f)
    }
}