package co.velloso.android.behaviors

import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior

fun View.getBehavior(): BottomSheetBehavior<*> = BottomSheetBehavior.from(this)

fun View.getLockableBehavior(): LockableBottomSheetBehavior<*> = LockableBottomSheetBehavior.from(this)