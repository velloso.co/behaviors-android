package co.velloso.android.behaviors

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.updatePadding
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior


class PaddingTopViewBehavior @JvmOverloads constructor(
        context: Context? = null,
        attrs: AttributeSet? = null
) : CoordinatorLayout.Behavior<AppBarLayout>(context, attrs) {

    private var originalPaddingTop: Int? = null

    private var childHeight: Int? = null

    private var parentHeight: Int? = null

    override fun layoutDependsOn(parent: CoordinatorLayout, child: AppBarLayout, dependency: View): Boolean {
        return isBottomSheet(dependency)
    }

    private fun isBottomSheet(dependency: View): Boolean {
        val params = dependency.layoutParams as? CoordinatorLayout.LayoutParams ?: return false
        return params.behavior is BottomSheetBehavior<*>
    }

    override fun onLayoutChild(parent: CoordinatorLayout, child: AppBarLayout, layoutDirection: Int): Boolean {
        childHeight = child.bottom - child.top
        parentHeight = parent.bottom - parent.top
        return super.onLayoutChild(parent, child, layoutDirection)
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: AppBarLayout, dependency: View): Boolean {
        if (originalPaddingTop == null) originalPaddingTop = dependency.paddingTop
        val progress = (parentHeight!! - dependency.top).toFloat() / parentHeight!!
        dependency.updatePadding(top = originalPaddingTop!! + childHeight!!.times(progress).toInt())
        return super.onDependentViewChanged(parent, child, dependency)
    }
}