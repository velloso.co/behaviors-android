package co.velloso.android.behaviors

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.View.GONE
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.updatePadding
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_HIDDEN
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt


class ContainerViewBehavior @JvmOverloads constructor(
        context: Context? = null,
        attrs: AttributeSet? = null
) : CoordinatorLayout.Behavior<View>(context, attrs) {

    private var bottomSheetHeight = 0

    private var bottomNavigationViewHeight = 0

    override fun layoutDependsOn(parent: CoordinatorLayout, child: View, dependency: View): Boolean {
        return isBottomNavigationView(dependency) || isBottomSheet(dependency)
    }

    private fun isBottomNavigationView(dependency: View): Boolean = dependency is BottomNavigationView

    private fun isBottomSheet(dependency: View): Boolean {
        val params = dependency.layoutParams as? CoordinatorLayout.LayoutParams ?: return false
        return params.behavior is BottomSheetBehavior<*>
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: View, dependency: View): Boolean {
        if (isBottomSheet(dependency)) {
            bottomSheetHeight = if (dependency.visibility != GONE &&
                    dependency.getBehavior().state != STATE_HIDDEN) min(
                    dependency.getBehavior().peekHeight + dependency.translationY.roundToInt(),
                    parent.bottom - dependency.top + dependency.translationY.roundToInt()
            ) else 0
        } else if (isBottomNavigationView(dependency)) {
            bottomNavigationViewHeight = parent.bottom - dependency.top
        }
        val shadowedArea = max(bottomSheetHeight, bottomNavigationViewHeight)
        if (shadowedArea != child.paddingBottom) child.updatePadding(bottom = shadowedArea)
        return super.onDependentViewChanged(parent, child, dependency)
    }
}