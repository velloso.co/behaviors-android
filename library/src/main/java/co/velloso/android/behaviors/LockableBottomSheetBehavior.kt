package co.velloso.android.behaviors

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior


class LockableBottomSheetBehavior<V : View> @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null
) : BottomSheetBehavior<V>(context, attrs) {

    var locked = false

    override fun onInterceptTouchEvent(parent: CoordinatorLayout, child: V, event: MotionEvent): Boolean {
        return if (locked) false
        else super.onInterceptTouchEvent(parent, child, event)
    }

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout, child: V, directTargetChild: View, target: View, axes: Int, type: Int): Boolean {
        return if (locked) false
        else super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, axes, type)
    }

    companion object {
        fun from(view: View): LockableBottomSheetBehavior<*> {
            val params = view.layoutParams
            if (params !is CoordinatorLayout.LayoutParams) {
                throw IllegalArgumentException("The view is not a child of CoordinatorLayout")
            } else {
                return params.behavior as? LockableBottomSheetBehavior<*>
                        ?: throw IllegalArgumentException("The view is not associated with LockableBottomSheetBehavior")
            }
        }
    }
}